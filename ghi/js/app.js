window.addEventListener('DOMContentLoaded', async () => {

    function createCard(name, description, pictureUrl, start, end, location) {
      return `
              <div class="card shadow-sm h-100">
                <img src="${pictureUrl}" class="card-img-top" style="aspect-ratio:4/3;" alt="...">
                <div class="card-body">
                  <h5 class="card-title">${name}</h5>
                  <h6 class="card-subtitle mb-4 text-secondary">${location}</h6>
                  <p class="card-text">${description}</p>
                </div>
                  <div class="card-footer"> ${humanTime(start)} - ${humanTime(end)}</div>
                </div>
              `
    }
  
    function createPlaceholder(index) {
      return `
          <div class="col-md-4 col-sm-12 placeholder-${index} mb-4">
            <div class="card shadow-sm" aria-hidden="true">
              <img class="card-img-top" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="placedholder" />
              <div class="card-body">
                <h5 class="card-title placeholder-glow">
                  <span class="placeholder col-6"></span>
                </h5>
                <h6 class="card-subtitle placeholder-glow mb-4">
                  <span class="placeholder col-7"></span>
                </h6>
                <p class="card-text placeholder-glow">
                  <span class="placeholder col-7"></span>
                  <span class="placeholder col-4"></span>
                  <span class="placeholder col-4"></span>
                  <span class="placeholder col-6"></span>
                  <span class="placeholder col-8"></span>
                </p>
              </div>
              <div class="card-footer"><span class="placeholder col-6"></span></div>
            </div>
          </div>
  
        `
    }
  
    function humanTime(datetime) {
      const date = new Date(datetime)
      const month = date.getMonth() + 1
      const day = date.getDate()
      const year = date.getFullYear()
      return `${month}/${day}/${year}`
    }
  
    function errrorMessage(message) {
      return `<div class="alert alert-danger">${message}</div>`
    }
  
    const url = 'http://localhost:8000/api/conferences/';
    try {
  
      const response = await fetch(url);
      if (!response.ok) {
        // Figure out what to do when the response is bad
        const column = document.querySelector('.error')
        const html = errrorMessage(`${response.status}, ${response.statusText} - Could not load conferences`)
        column.innerHTML += html;
      } else {
        const data = await response.json();
  
        for (let i = 0; i < data.conferences.length; i++) {
          const html = createPlaceholder(i)
          const column = document.querySelector('.row')
          column.innerHTML += html;
  
        }
        for (let i = 0; i < data.conferences.length; i++) {
          const conference = data.conferences[i]
          const detailUrl = `http://localhost:8000${conference.href}`
          const detailResponse = await fetch(detailUrl)
          if (detailResponse.ok) {
            const details = await detailResponse.json()
            const title = details.conference.name
            const description = details.conference.description
            const pictureUrl = details.conference.location.picture_url
            const start = details.conference.starts
            const end = details.conference.ends
            const location = details.conference.location.name
            const html = createCard(title, description, pictureUrl, start, end, location)
            const column = document.querySelector(`.placeholder-${i}`)
            // console.log(details)
            column.innerHTML = html;
            column.classList.remove(`placeholder-${i}`)
          }
        }
      }
    } catch (e) {
      // Figure out what to do if an error is raised
      const column = document.querySelector('.error')
      const html = errrorMessage(`${e}`)
      column.innerHTML += html;
    }
  
  });
  
  